package fr.surnami.tools;

import junit.framework.TestCase;

import static com.codeborne.selenide.Selenide.$;
import org.openqa.selenium.By;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.firefox.*;


public class HeadlessBrowserTest extends TestCase {
	private HeadlessBrowser headlessBrowser;
	private FirefoxOptions firefoxOptions;
	private String className;
	private String url;
	public void setUp() {
		try {
			className = "corp-bclogo";
			url = "https://bandcamp.com";
			headlessBrowser = (HeadlessBrowser) Class
												.forName("fr.surnami.tools.HeadlessBrowser")
												.newInstance();
			firefoxOptions = new FirefoxOptions();
			firefoxOptions.addArguments("--headless", "--disable-gpu", "--window-size=1600,900", "--ignore-certificate-errors", "--silent");
			firefoxOptions.setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.IGNORE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void testCreateTest() {
		assertNotNull(headlessBrowser);
	}
	

	public void testHeadlessBrowserOptions() {
		assertEquals("[acceptInsecureCerts, browserName, unhandledPromptBehavior]", headlessBrowser.getOptions().getCapabilityNames().toString());
	}
	
	public void testHeadlessBrowserDriver() {
		assertNotNull(headlessBrowser.getDriver());
	}
	
	public void testHeadlessBrowserNavigation() {
		
		headlessBrowser.visit(url);
		assertEquals("Bandcamp", $(By.className(className)).getText());
	}
}
