package fr.surnami.tools;


import com.codeborne.selenide.ElementsCollection;

import junit.framework.TestCase;

public class AnalyzerTest extends TestCase{
	private Analyzer analyzer;
	private ElementsCollection elements;
	
	public void setUp() {
		try {
			analyzer = (Analyzer) Class.forName("fr.surnami.tools.Analyzer").newInstance();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void testCreateClass() {
		assertNotNull(analyzer);
	}
	
	public void testBuildList() {
		analyzer.extract(elements);	
	}
	
	
}
