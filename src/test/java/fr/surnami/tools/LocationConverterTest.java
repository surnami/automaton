package fr.surnami.tools;

import static com.codeborne.selenide.Selenide.*;

import org.openqa.selenium.By;

import fr.surnami.tools.LocationConverter;
import junit.framework.TestCase;

public class LocationConverterTest extends TestCase {
	private static final String CSS_BUTTON_SUBMIT_ID = "input:nth-child(4)";
	private static final String INPUT_SEARCH_ID = "q";
	private static final String FIRST_LINK = ".restable > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(2) > a:nth-child(1)";
	private LocationConverter locationConverter;
	private String homepage;
	private String location;
	private String[] coordinates = new String[2];
	
	public void setUp() {
		try {
			locationConverter = (LocationConverter) Class
					.forName("fr.surnami.tools.LocationConverter")
					.newInstance();
			homepage = LocationConverter.LAT_LONG_HOMEPAGE;
			locationConverter.reachLocationConverter();
			location = "Issou, France";
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void testCreate() {
		assertNotNull(locationConverter);
	}
	
	public void testReachWebsite() {
		assertEquals("GeoNames", $(By.xpath(LocationConverter.XPATH_HEADER)).innerText());
	}
	
	public void testLaunchingConversion() {
		assertNotNull( $(By.name(INPUT_SEARCH_ID)) );
		$(By.name(INPUT_SEARCH_ID)).sendKeys(location);
		
		assertNotNull( $(By.cssSelector(CSS_BUTTON_SUBMIT_ID)) );
		$(By.cssSelector(CSS_BUTTON_SUBMIT_ID)).click();
	}
	
	public void testObtainCoordinates() {
		testLaunchingConversion();
		assertEquals("Issou", $(By.cssSelector(FIRST_LINK)).innerText() ) ;
		$(By.cssSelector(FIRST_LINK)).click();
		assertEquals( 2, $$(By.cssSelector(".form-inline > span.edit-mode.move-mode")).size());
	}
	
}
