package fr.surnami.tools;

import static com.codeborne.selenide.Selenide.*;
import org.openqa.selenium.By;
import junit.framework.TestCase;

public class ScraperTest extends TestCase {
    private Scraper scraper;
    
	public void setUp() {
	   try {
		   scraper = (Scraper) Class.forName("fr.surnami.tools.Scraper").newInstance();
	   } catch (Exception e) {
		   e.printStackTrace();
	   }
	   
    }
	
	public void testSearchBox() {
		scraper.fillIn();
		assertNotNull($(By.xpath("/html/body/div[4]/div/div[1]/div[1]/div[2]/div/p/a")) );
		scraper.launchSearch();
	}
	
	public void testRefiningResults() {
		scraper.selectAllReleases();
		scraper.selectSortingOption();
		scraper.addTag();
		assertNotNull( $(By.cssSelector(".view-more")) );
	}
	
}
