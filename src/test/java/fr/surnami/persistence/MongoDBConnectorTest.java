package fr.surnami.persistence;

import static org.junit.Assert.assertNotNull;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import junit.framework.TestCase;

public class MongoDBConnectorTest extends TestCase {
	private MongoDatabase mongoDatabase;
	private MongoCollection<?> mongoCollection;
	private MongoDBConnector mongoDBConnector;
	
	
	public void setUp() {
		try {
			mongoDBConnector = (MongoDBConnector) Class
					.forName("fr.surnami.persistence.MongoDBConnector")
					.newInstance();
			mongoDatabase = MongoDBConnector.MONGO_DATABASE;
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void testCreate() {
		assertNotNull(mongoDBConnector);
	}
	
	public void testAccessingDatabase(){
		assertEquals(System.getenv("ATLAS_DB"), mongoDatabase.getName());
    }
	
	public void testFetchCollection() {
		mongoCollection = mongoDBConnector.fetch("artists");
		assertEquals("artists", mongoDatabase.getCollection("artists").getNamespace().getCollectionName());
	}
}
