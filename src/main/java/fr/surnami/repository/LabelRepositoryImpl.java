package fr.surnami.repository;

import org.bson.conversions.Bson;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import fr.surnami.model.Label;
import fr.surnami.persistence.MongoDBConnector;

public class LabelRepositoryImpl implements LabelRepository {

	@Override
	public void insertCreate(Label label) {
		try {
			MongoCollection<Label> labels = MongoDBConnector.MONGO_DATABASE.getCollection("labels", Label.class);
			labels.insertOne(label);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
	}

	@Override
	public void upsert(Label label) {
		try {
			String labelName = label.getName();
			Bson filter = Filters.eq("name", labelName);
			MongoCollection<Label> artists = MongoDBConnector.MONGO_DATABASE.getCollection("labels", Label.class);
			artists.replaceOne(filter, label);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(Label label) {
		try {
			String labelName = label.getName();
			Bson filter = Filters.eq("name", labelName);
			MongoCollection<Label> labels = MongoDBConnector.MONGO_DATABASE.getCollection("labels", Label.class);
			labels.deleteOne(filter);
		} catch (Exception e) {
			e.printStackTrace();
		
		}
		
	}

}
