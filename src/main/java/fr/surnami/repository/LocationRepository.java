package fr.surnami.repository;

import fr.surnami.model.Location;

public interface LocationRepository extends GenericRepository<Location>{
	public void upsert (Location location);
	public void delete (Location location);
}
