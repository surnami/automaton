package fr.surnami.repository;

import com.mongodb.client.MongoCollection;

import fr.surnami.model.Album;
import fr.surnami.persistence.MongoDBConnector;

public class AlbumRepositoryImpl implements AlbumRepository {
	@Override
	public void insertCreate(Album album) {
		try {
			MongoCollection<Album> albums = MongoDBConnector.MONGO_DATABASE.getCollection("albums", Album.class);
			albums.insertOne(album);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
}
