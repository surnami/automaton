package fr.surnami.repository;

import org.bson.conversions.Bson;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;

import fr.surnami.model.Artist;
import fr.surnami.persistence.MongoDBConnector;

public class ArtistRepositoryImpl implements ArtistRepository{

	@Override
	public void insertCreate(Artist artist) {
		try {
			MongoCollection<Artist> artists = MongoDBConnector.MONGO_DATABASE.getCollection("artists", Artist.class);
			artists.insertOne(artist);
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
	}

	@Override
	public void upsert(Artist artist) {
		try {
			String artistName = artist.getName();
			Bson filter = Filters.eq("name", artistName);
			MongoCollection<Artist> artists = MongoDBConnector.MONGO_DATABASE.getCollection("artists", Artist.class);
			artists.replaceOne(filter, artist);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
	}

	@Override
	public void delete(Artist artist) {
		try {
			String artistName = artist.getName();
			Bson filter = Filters.eq("name", artistName);
			MongoCollection<Artist> artists = MongoDBConnector.MONGO_DATABASE.getCollection("artists", Artist.class);
			artists.deleteOne(filter);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
	}



}
