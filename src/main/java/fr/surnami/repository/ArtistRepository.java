package fr.surnami.repository;

import fr.surnami.model.Artist;

public interface ArtistRepository extends GenericRepository<Artist>{
	public void upsert(Artist artist);
	public void delete(Artist artist);
}
