package fr.surnami.repository;

import org.bson.conversions.Bson;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;

import fr.surnami.model.Location;
import fr.surnami.persistence.MongoDBConnector;

public class LocationRepositoryImpl implements LocationRepository {

	@Override
	public void insertCreate(Location location) {
		try {
			MongoCollection<Location> locations = MongoDBConnector.MONGO_DATABASE.getCollection("locations", Location.class);
			locations.insertOne(location);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
	}

	@Override
	public void upsert(Location location) {
		try {
			String locationName = location.getName();
			Bson filter = Filters.eq("name", locationName);
			//Bson operation = push();
			MongoCollection<Location> locations = MongoDBConnector.MONGO_DATABASE.getCollection("locations", Location.class);
			//locations.updateOne(filter, location);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(Location location) {
			
	}

}
