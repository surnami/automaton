package fr.surnami.repository;

import com.mongodb.client.MongoCollection;
import fr.surnami.model.Network;
import fr.surnami.persistence.MongoDBConnector;

public class NetworkRepositoryImpl implements NetworkRepository{

	@Override
	public void insertCreate(Network network) {
		try {
			MongoCollection<Network> networks = MongoDBConnector.MONGO_DATABASE.getCollection("networks", Network.class);
			networks.insertOne(network);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
	}

}
