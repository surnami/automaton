package fr.surnami.repository;

import fr.surnami.model.Label;

public interface LabelRepository extends GenericRepository<Label>{
	public void upsert (Label label);
	public void delete (Label label);
}
