package fr.surnami.repository;

import fr.surnami.model.Network;

public interface NetworkRepository extends GenericRepository<Network> {

}
