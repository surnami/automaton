package fr.surnami.tools;

import static com.codeborne.selenide.Selenide.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codeborne.selenide.ElementsCollection;

import fr.surnami.tools.HeadlessBrowser;
import lombok.*;

@Data @AllArgsConstructor @NoArgsConstructor @ToString
public class LocationConverter {
	public static final Logger LOGGER = LoggerFactory.getLogger(LocationConverter.class);
	public static final String LAT_LONG_HOMEPAGE = "http://www.geonames.org/";
	public static final String XPATH_HEADER = "/html/body/div[2]/h1";
	public static final String INPUT_SEARCH_ID = "q";
	private static final String CSS_BUTTON_SUBMIT_ID = "input:nth-child(4)";
	private static final String CSS_FIRST_LINK = ".restable > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(2) > a:nth-child(1)";
	static final String CSS_SPAN = "span.edit-mode.move-mode";
	private static final String CSS_COORDINATES = ".form-inline > span.edit-mode";
	private HeadlessBrowserFactory headlessBrowser;
	private ElementsCollection coordinates;
	private String location;
	private String latitude;
	private String longitude;
	
	public void reachLocationConverter() {
		try {
			headlessBrowser = (HeadlessBrowser) Class
					.forName("fr.surnami.tools.HeadlessBrowser")
					.newInstance();
			LOGGER.info("Reaching location converter...");
			headlessBrowser.visit(LAT_LONG_HOMEPAGE);
		} catch (Exception e) {
			LOGGER.error("Failure occured: " + e.getMessage());
		}
	}
	
	public void launchConverting(String location) {
		try {
			LOGGER.info("Entering location: " + location + " into the input field");
			$(By.name(INPUT_SEARCH_ID)).sendKeys(location);
			
			WebDriverWait wait = new WebDriverWait (headlessBrowser.getDriver(), (long) (Math.random()*20));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(CSS_BUTTON_SUBMIT_ID)));
			
			LOGGER.info("Clicking on submit...");
			$(By.cssSelector(CSS_BUTTON_SUBMIT_ID)).click();
		} catch (Exception e) {
			LOGGER.error("Error detected while fetching coordinates");
		}
	}
	
	public void fetchCoordinates() {
		try {
			
		WebDriverWait wait = new WebDriverWait (headlessBrowser.getDriver(), (long) (Math.random()*0));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(CSS_FIRST_LINK)));
		
		LOGGER.info("Opening the result page...");
		$(By.cssSelector(CSS_FIRST_LINK)).click();
		coordinates = $$(By.cssSelector(CSS_COORDINATES));
	 
		latitude = coordinates.get(0).innerText();
		LOGGER.info("Registered latitude: " + latitude);
		longitude = coordinates.get(1).innerText();
		LOGGER.info("Registered longitude: " + longitude);
		} catch(Exception e) {
			LOGGER.error("Error detected while fetching coordinates - " + e.getMessage());
		}
	}

}
