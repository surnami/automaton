package fr.surnami.tools;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;
import org.slf4j.*;

import static com.codeborne.selenide.Selenide.*;

import java.util.*;
import com.codeborne.selenide.WebDriverRunner;
import lombok.*;


@Data @NoArgsConstructor @ToString
public class HeadlessBrowser implements HeadlessBrowserFactory {
	private static final Logger LOGGER = LoggerFactory.getLogger(HeadlessBrowser.class);
	private static final String WEBDRIVER_BIN = "webdriver.firefox.bin";
	private static final String FIREFOX_PATH = "/automaton/firefox";
	private static final String WEBDRIVER = "webdriver.gecko.driver";
	private static final String SELENIDE_BROWSER = "selenide.browser";
	
	public static final String DRIVER_LOCATION = "/automaton/geckodriver";
	private static final List<String> OPTIONS_ARGUMENTS = 
			Arrays.asList("--headless", "--disable-gpu", "--window-size=1600,900", "--ignore-certificate-errors", "--silent", "--incognito");
	
	private static final String BROWSER = "Firefox";
	private static final FirefoxOptions FIREFOX_OPTIONS = new FirefoxOptions().addArguments(OPTIONS_ARGUMENTS);
	private static final FirefoxDriver FIREFOX_DRIVER = new FirefoxDriver(FIREFOX_OPTIONS.setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.ACCEPT));
	private static final JavascriptExecutor JAVASCRIPT_EXECUTOR = (JavascriptExecutor) FIREFOX_DRIVER;
	
	@Override
	public FirefoxDriver getDriver() {
		LOGGER.info("Setting up driver...");
		System.setProperty(WEBDRIVER_BIN, FIREFOX_PATH);
		System.setProperty( WEBDRIVER, DRIVER_LOCATION );
		System.setProperty(SELENIDE_BROWSER, BROWSER);
		LOGGER.info("Fetching driver...");
		return (FirefoxDriver) JAVASCRIPT_EXECUTOR;
	}

	@Override
	public FirefoxOptions getOptions() {
		LOGGER.info("Setting up options...");
		return FIREFOX_OPTIONS;
	}

	@Override
	public void pause(Long durationMax) {
		try {
			LOGGER.info("Pausing scraper up to " + durationMax + " milliseconds");
			Thread.sleep((long) (Math.random()*durationMax));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void visit(String url) {
		LOGGER.info("Reaching " + url);
		WebDriverRunner.setWebDriver(getDriver());
		open(url);
	}


}
