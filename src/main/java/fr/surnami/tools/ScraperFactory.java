package fr.surnami.tools;

public interface ScraperFactory {
	public void fillIn();
	public void launchSearch();
}
