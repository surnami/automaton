package fr.surnami.tools;

import static com.codeborne.selenide.Selenide.*;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.slf4j.*;

import com.codeborne.selenide.ElementsCollection;

import lombok.*;


@Data @AllArgsConstructor @NoArgsConstructor @ToString
public class Scraper implements ScraperFactory {
	private static final Logger LOGGER = LoggerFactory.getLogger(Scraper.class);
	private static final String BANDCAMP_HOMEPAGE = "https://bandcamp.com";
	
	private static final String INPUT_SEARCH_NAME = "q";
	public static final String LINK_TEXT = "music tagged with “vaporwave.”";
	private static final String QUERY_VALUE = "vaporwave";
	private static final String QUERY_VALUE_TAG = "electronic";
	
	private static final String CSS_SELECTOR_SEARCH_BUTTON = "button > .menubar-search-icon";
	private static final String CSS_SELECTOR_ALL_RELEASES = ".tab:nth-child(2)";
	private static final String CSS_SELECTOR_SORT_FILTER = "#show-filter-select-sort > span";
	private static final String CSS_SELECTOR_NEW_ARRIVALS = ".expanded > li:nth-child(2)";
	private static final String CSS_SELECTOR_RELEVANT_TAG = "#show-filter-select-tags > span";
	private static final String CSS_SELECTOR_INPUT_TAG = "div:nth-child(2) > input";
	private static final String CSS_SELECTOR_VIEW_MORE = ".view-more";
	
	private static final String SCRIPT_ACTIVATE_TAGS = "'arguments[0].style = \"\"; "
			   + "arguments[0].style.display = \"block\"; "
			   + "arguments[0].style.visibility = \"visible\";'"
			   + ",input_element";
	
	private static final String SCRIPT_SCROLL_DOWN = "window.scrollTo(0, document.body.scrollHeight)";
	
	private static final int INITIAL_HEIGHT=0;
	private static final int FINAL_HEIGHT=2000;
	private static final String XPATH_ALBUM_DIV = "//*[@id=\"dig-deeper\"]/div[2]/div[1]/div";
	
	private HeadlessBrowserFactory headlessBrowser;
	private ElementsCollection albumDivs;
	
	
	@Override
	public void fillIn() {
		try {
			visit();
			LOGGER.info("Typing '" + QUERY_VALUE + "' in the search field...");
			headlessBrowser = (HeadlessBrowser) Class.forName("fr.surnami.tools.HeadlessBrowser").newInstance();
			$(By.name(INPUT_SEARCH_NAME)).sendKeys(QUERY_VALUE);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
	}
    
	@Override
	public void launchSearch() {
		try {
			LOGGER.info("Clicking on the search button");
			click(CSS_SELECTOR_SEARCH_BUTTON);
			LOGGER.info("Clicking on the 'music tagged vaporwave'link");
			click(LINK_TEXT);
		} catch(Exception e) {
			LOGGER.error(e.getMessage());
		}
	}

	public void selectAllReleases() {
		try {
			LOGGER.info("Select all releases");
			pause(600);
			click(CSS_SELECTOR_ALL_RELEASES);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
	}

	public void addTag() {
		try {
			jsExecute(SCRIPT_ACTIVATE_TAGS);
			LOGGER.info("Adding supplemental tag: " + QUERY_VALUE_TAG);
			pause(550);
			sendKeysTo(QUERY_VALUE_TAG, CSS_SELECTOR_INPUT_TAG);
		} catch(Exception e) {
			LOGGER.error(e.getMessage());
		}
	}
	
	public void selectSortingOption() {
		try {
			LOGGER.info("Picking the relevant sorting option");
			pause(600);
			click(CSS_SELECTOR_SORT_FILTER);
			pause(400);
			click(CSS_SELECTOR_NEW_ARRIVALS);
			pause(300);
			moveTo(CSS_SELECTOR_RELEVANT_TAG);
			pause(500);
			click(CSS_SELECTOR_RELEVANT_TAG);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
	}

	public void expandResults( ) {
		try {
			LOGGER.info("Expanding result page");
			pause(750);
			click(CSS_SELECTOR_VIEW_MORE);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
	}
	
	public void scrollDown() {
		try {
			LOGGER.info("Scrolling down the single page result...");
			int height = INITIAL_HEIGHT;
			while (height  < FINAL_HEIGHT) {
				LOGGER.info("...scrolling down...");
				pause(2000);
				jsExecute(SCRIPT_SCROLL_DOWN);
				height += Math.random()*1500;
			}
			fetch();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		
	}
	
	private void fetch() {
		albumDivs = $$( By.xpath(XPATH_ALBUM_DIV));
	}
	
	private void jsExecute(String script) {
	    headlessBrowser
	   .getDriver()
	   .executeScript(script);
	}
	
	private void moveTo(String clickedElement) {
		 WebElement element = $(By.cssSelector(clickedElement));
	     Actions builder = new Actions(headlessBrowser.getDriver());
	     builder.moveToElement(element).perform();
	}
	
	private void sendKeysTo(String queryValueTag, String cssSelectorInputTag) {
		$(By.cssSelector(cssSelectorInputTag)).sendKeys(queryValueTag+Keys.ENTER);
	}


	private void click(String element) {
	    if (element.contains("music")) {
	    	$(By.linkText("music tagged with “vaporwave.”")).click();
	    } else {
	    	$(By.cssSelector(element)).click();
	    }
	}
	
	private void pause(long duration) {
		headlessBrowser.pause((long) duration);
	}
	
	private void visit() {
		try {
			headlessBrowser = (HeadlessBrowser) Class.forName("fr.surnami.tools.HeadlessBrowser").newInstance();
			headlessBrowser.visit(BANDCAMP_HOMEPAGE);
			pause(450);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	


}
