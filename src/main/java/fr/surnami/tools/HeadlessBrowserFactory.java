package fr.surnami.tools;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public interface HeadlessBrowserFactory {
	public FirefoxDriver getDriver();
	public FirefoxOptions getOptions();
	public void pause(Long durationMax);
	public void visit(String url);
}
