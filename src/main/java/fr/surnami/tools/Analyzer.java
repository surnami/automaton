package fr.surnami.tools;

import java.util.*;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.*;
import org.slf4j.*;

import com.codeborne.selenide.ElementsCollection;

import fr.surnami.repository.*;
import fr.surnami.model.*;
import lombok.*;

@Data @AllArgsConstructor @ToString
public class Analyzer {
	private static final Logger LOGGER = LoggerFactory.getLogger(Analyzer.class);
	private static final String TYPE_BANDCAMP = "bandcamp";
	private static final String LOCATION_CSS_SELECTOR = ".location.secondaryText";
	
	private HeadlessBrowserFactory headlessBrowser;
	private Artist artist;
	private Album album;
	private Label label;
	private Location location;
	private Network network;

	private LocationConverter locationConverter;
	private NetworkRepository networkRepository;
	private AlbumRepository albumRepository;
	private LabelRepository labelRepository;
	private LocationRepository locationRepository;
	private ArtistRepository artistRepository;

	public Analyzer(

	) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
		networkRepository = (NetworkRepositoryImpl) Class.forName("fr.surnami.repository.NetworkRepositoryImpl").newInstance();
		albumRepository = (AlbumRepositoryImpl) Class.forName("fr.surnami.repository.AlbumRepositoryImpl").newInstance();
		locationRepository = (LocationRepositoryImpl) Class.forName("fr.surnami.repository.LocationRepositoryImpl").newInstance();
		artistRepository = (ArtistRepositoryImpl) Class.forName("fr.surnami.repository.ArtistRepositoryImpl").newInstance();
		labelRepository = (LabelRepositoryImpl) Class.forName("fr.surnami.repository.LabelRepositoryImpl").newInstance();
	}
	
	public void extract(ElementsCollection elements) {
		try {
			List<Network> networkArtist = new ArrayList<>();
			List<Network> networkLabel = new ArrayList<>();
			List<String> bandcampAccounts = new ArrayList<>();

			List<Artist> artists = new ArrayList<>();
			List<Label> labels = new ArrayList<>();

			int counter = 0;
			int numberOfArtists = 0;
			int numberOfLabels = 0;

			boolean isArtistURL = false;
			List<Boolean> arrayIsArtistURL = new ArrayList<>();


			locationConverter = (LocationConverter) Class
					.forName("fr.surnami.tools.LocationConverter")
					.newInstance();

			LOGGER.info("Setting up headless browser...");
			headlessBrowser = (HeadlessBrowser) Class.forName("fr.surnami.tools.HeadlessBrowser").newInstance();
			
			LOGGER.info("Extracting data from results...");
			for (SelenideElement e: elements) {
				LOGGER.info("Fetching models...");
				artist = (Artist) Class.forName("fr.surnami.model.Artist").newInstance();
				album = (Album) Class.forName("fr.surnami.model.Album").newInstance();
				label = (Label) Class.forName("fr.surnami.model.Label").newInstance();
				network = (Network) Class.forName("fr.surnami.model.Network").newInstance();

				String[] URL = e.findElement(By.tagName("a")).getAttribute("href").split("album");
				String bandcampAccountURL = URL[0];
				bandcampAccounts.add(bandcampAccountURL);
				
				LOGGER.info("Fetching album title and artist name");
				String[] titleAndArtist = e.getText().split("by");
				
				String titleAlbum = titleAndArtist[0].trim();
				String artistName = titleAndArtist[1].trim();
				
				LOGGER.info("Setting album title");
				album.setName(titleAlbum);
				
				LOGGER.info("Setting album sleeve design URL");
				String sleeveDesignURL = e.findElement(By.tagName("img")).getAttribute("src");
                album.setSleeveDesignURL(sleeveDesignURL);
				
				LOGGER.info("Setting network type and URL");
				network.setName(TYPE_BANDCAMP);
				network.setUrl(bandcampAccountURL);
				
				LOGGER.info("Inserting network in the database");
				networkRepository.insertCreate(network);
				
				LOGGER.info("Setting artist name");
			    artist.setName(artistName);
			    
				LOGGER.info("Setting album artist");
				album.setArtist(artist);
				
			    
			    if (urlChecker(artistName, bandcampAccountURL)) {
					
					LOGGER.info("Setting artist network");
					artist.setNetworks(networkArtist);
					
					LOGGER.info("Adding network type to the network list");
					networkArtist.add(network);
					
					LOGGER.info("Inserting album in the database");
					albumRepository.insertCreate(album);
					
					artists.add(artist);
					
					isArtistURL = true;
					arrayIsArtistURL.add(true);
					
					networkArtist.clear();
				}
				
			    if (!urlChecker(artistName, bandcampAccountURL)) {

					LOGGER.info("Setting label network");
					label.setNetworks(networkLabel);
					
					LOGGER.info("Adding network type to the network list");
					networkLabel.add(network);
					
					LOGGER.info("Setting label name");
					label.setName(makeNameOutOfURL(bandcampAccountURL));
					
					LOGGER.info("Setting album label");
					album.setLabel(label);
					
					LOGGER.info("Inserting album in the database");
					albumRepository.insertCreate(album);
					
					labels.add(label);
					
					isArtistURL = false;
					arrayIsArtistURL.add(false);
					networkLabel.clear();
				}
					
				if (isArtistURL) {
					isArtistURL = false;
				}
			}
			
			for (String bandcampAccountLink: bandcampAccounts) {
				LOGGER.info("Fetching models...");

				headlessBrowser.visit(bandcampAccountLink+"music");
				location = (Location) Class.forName("fr.surnami.model.Location").newInstance();
				LOGGER.info("Defining location... ");
				String locationName = defineLocation();

				if (!locationName.isEmpty()) {
					LOGGER.info("Setting location name to " + locationName);
					location.setName(locationName);

					LOGGER.info("Translating location name to GPS coordinates... ");
					locationConverter.reachLocationConverter();
					locationConverter.launchConverting(locationName);
					locationConverter.fetchCoordinates();
					LOGGER.info("Location latitude set to " + locationConverter.getLatitude());
					location.setLatitude(locationConverter.getLatitude());

					LOGGER.info("Location longitude set to " + locationConverter.getLongitude());
					location.setLongitude(locationConverter.getLongitude());

					boolean isArtist = arrayIsArtistURL.get(counter);

					if (isArtist && (numberOfArtists < artists.size())) {
						LOGGER.info("Assigning artist: " + artists.get(numberOfArtists).getName()
								+ " to " + location);

						location.getArtists().add(artists.get(numberOfArtists));
						artists.get(numberOfArtists).setLocation(location);

						LOGGER.info("Inserting location in the database");
						locationRepository.insertCreate(location);
						location.getArtists().clear();
						numberOfArtists++;
						counter++;
					}


					if (!isArtist && (numberOfLabels < labels.size())) {
						LOGGER.info("Assigning label: " + labels.get(numberOfLabels).getName()
								+ " to " + location);
						location.getLabels().add(labels.get(numberOfLabels));

						labels.get(numberOfLabels).setLocation(location);
						LOGGER.info("Inserting location in the database");
						locationRepository.insertCreate(location);
						location.getLabels().clear();
						numberOfLabels++;
						counter++;
					}

				}
			    
			}
			
		} catch(Exception e) {
			LOGGER.error("Extraction failure: " + e.getMessage());
			e.printStackTrace();
		}
		
		
		
	}

	private String defineLocation() {
		String locationName = "";
		try {
		locationName = headlessBrowser
				.getDriver()
				.findElement( By.cssSelector(LOCATION_CSS_SELECTOR) )
				.getText();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return locationName;
	}
	
	private boolean urlChecker(String artistName, String url) {
		String formattedArtistName = artistNameFormatter(artistName);
		return url.contains(formattedArtistName);
	}

	private String artistNameFormatter(String artistName) {
		String[] valuesToFormat = artistName.split(" ");
		StringBuilder buffer = new StringBuilder();
		for(String s: valuesToFormat) {
			buffer.append(s.toLowerCase());
		}
		return buffer.toString();
	}

	private String makeNameOutOfURL(String url) {
		String[] temporaryArray = url.split("\\.");
		return temporaryArray[0].substring(8);
	}


}
