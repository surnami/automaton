package fr.surnami.persistence;


import static org.bson.codecs.configuration.CodecRegistries.*;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import com.mongodb.*;
import com.mongodb.client.*;
import com.mongodb.client.MongoClient;

import fr.surnami.model.Artist;
import lombok.*;

@Data @NoArgsConstructor @ToString
public class MongoDBConnector {
	
	private static final String URI = "mongodb+srv://caliceglass:" 
		+ System.getenv("ATLAS_PWD") 
		+ "@cluster0.2irtk.mongodb.net/" 
		+ System.getenv("ATLAS_DB")  
		+ "?authSource=admin&compressors=zlib&"
		+ "gssapiServiceName=mongodb&retryWrites=true&w=majority";
	
	private static final ConnectionString CONNECTION_STRING = new ConnectionString(URI);
	private static final CodecRegistry POJO_CODEC_REGISTRY = fromProviders(
			PojoCodecProvider.builder().automatic(true).build()
	);
	
	private static final CodecRegistry CODEC_REGISTRY = fromRegistries(
			MongoClientSettings.getDefaultCodecRegistry(), POJO_CODEC_REGISTRY
	);
	
	private static final MongoClientSettings CLIENT_SETTINGS = MongoClientSettings.builder()
            .applyConnectionString(CONNECTION_STRING)
            .codecRegistry(CODEC_REGISTRY)
            .build();
	
	private static final MongoClient MONGO_CLIENT = MongoClients.create(CLIENT_SETTINGS);
	public static final MongoDatabase MONGO_DATABASE = 
			MONGO_CLIENT.getDatabase(System.getenv("ATLAS_DB"));

	public MongoCollection<?> fetch(String collectionName) {
		return MONGO_DATABASE.getCollection(collectionName);
	}
}
