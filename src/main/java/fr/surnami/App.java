package fr.surnami;
import org.slf4j.*;

import fr.surnami.persistence.MongoDBConnector;
import fr.surnami.tools.*;

public class App {
	private static final Logger LOGGER = LoggerFactory.getLogger(App.class);
	
	public static void main(String[] args) {
		try {
			Scraper scraper = (Scraper) Class
					.forName("fr.surnami.tools.Scraper")
					.newInstance();
			scraper.fillIn();
			scraper.launchSearch();
			scraper.selectAllReleases();
			scraper.addTag();
			scraper.selectSortingOption();
			scraper.expandResults();
		    scraper.scrollDown();
		
		    Analyzer analyzer = (Analyzer) Class
		    		.forName("fr.surnami.tools.Analyzer")
		    		.newInstance();
		    analyzer.extract(scraper.getAlbumDivs());
		    

		} catch(Exception e) {
			LOGGER.error(e.getMessage());
		}
		
		
	}

}
