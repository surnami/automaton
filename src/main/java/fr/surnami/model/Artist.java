package fr.surnami.model;

import java.util.*;
import fr.surnami.model.*;
import lombok.*;
import org.bson.codecs.pojo.annotations.*;

@Data @AllArgsConstructor @EqualsAndHashCode(callSuper = true) @ToString(exclude = {"location", "networks"})
public class Artist extends GenericModel<Artist> {

	private List<Network> networks = new ArrayList<>();
	@BsonIgnore
	private Location location;

	public Artist() {
		super();
	}

	public Artist( String name, List<Network> networks, Location location) {
		super(name);
		this.networks = networks;
		this.location = location;
	}

}
