package fr.surnami.model;

import java.util.*;

import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonIgnore;

import fr.surnami.model.*;
import lombok.*;

@Data @AllArgsConstructor @EqualsAndHashCode(callSuper = true)
public class Location extends GenericModel<Location> {

	private String latitude;
	private String longitude;
	private List<Artist> artists = new ArrayList<>();
	private List<Label> labels = new ArrayList<>();

	public Location() {
		super();
	}

	public Location( String name, String latitude, String longitude, List<Artist> artists, List<Label> labels) {
		super(name);
		this.latitude = latitude;
		this.longitude = longitude;
		this.artists = artists;
		this.labels = labels;
	}
}
