package fr.surnami.model;

import org.bson.codecs.pojo.annotations.BsonProperty;
import fr.surnami.model.*;
import lombok.*;


@Data @AllArgsConstructor @EqualsAndHashCode(callSuper = true)
public class Album extends GenericModel<Album> {
	
	@BsonProperty(value = "sleeve_Design_URL")
	private String sleeveDesignURL;
	private Artist artist;
	private Label label;
	
	public Album() { super(); }
	
	public Album(String name, String sleeveDesignURL, Artist artist, Label label ) {
		super(name);
		this.sleeveDesignURL = sleeveDesignURL;
		this.artist = artist;
		this.label = label;
	}
}
