package fr.surnami.model;

import java.util.*;
import org.bson.codecs.pojo.annotations.*;
import fr.surnami.model.*;
import lombok.*;

@Data @EqualsAndHashCode(callSuper = true) @ToString(exclude = {"location", "networks"})
public class Label extends GenericModel<Label> {

	private List<Network> networks = new ArrayList<Network>();
	@BsonIgnore
	private Location location;

	public Label() { super(); }

	public Label (String name, List<Network> networks, Location location ) {
		super(name);
		this.networks = networks;
		this.location = location;
	}
	
}
