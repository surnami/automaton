package fr.surnami.model;

import lombok.*;
import org.bson.codecs.pojo.annotations.BsonId;

@Data @AllArgsConstructor @EqualsAndHashCode(callSuper = true)
public class Network extends GenericModel<Network>{
	private String url;

	public Network() { super();}
	public Network(String name, String url) {
		super(name);
		this.url = url;
	}
}
