package fr.surnami.model;

import org.bson.types.ObjectId;

import lombok.*;


@Data @ToString
public abstract class GenericModel<T> {
	private ObjectId id;
	private String name;
	
	public GenericModel() {
		id = new ObjectId();
	}
	
	public GenericModel(String name) {
		id = new ObjectId();
		this.name = name;
	}
}
