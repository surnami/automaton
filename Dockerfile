FROM openjdk:8-jre-alpine

LABEL name="maven/jdk8/alpine container"
LABEL version="1.0"
LABEL architecture="x86_64"

RUN apk update
RUN apk --no-cache add firefox-esr

WORKDIR /home

COPY . .

ARG TEST_A
ENV ATLAS_DB=TEST_A
ARG TEST_B
ENV ATLAS_PWD=TEST_B

RUN adduser -D nnyimc
USER nnyimc

CMD [ "sh", "-c", "java -Dserver.port=$PORT -Xmx300m -Xss512k -XX:CICompilerCount=2 -Dfile.encoding=UTF-8 -XX:+UseContainerSupport -Djava.security.egd=file:/dev/./urandom -jar out/artifacts/automaton_jar/automaton.jar"]
